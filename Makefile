default:
	cargo run --

build:
	cargo build --release

debug:
	RUST_LOG=debug cargo run --

test:
	cargo test
