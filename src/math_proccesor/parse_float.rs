use mexprp::{Expression, Context};
use std::collections::HashMap;

pub fn run(format: &str, variables: &HashMap<String, f64>) -> f64{
    let mut ctx: Context<f64> = Context::new();

    for (key, value) in variables {
        ctx.set_var(key, *value);
    }

    let expr: Expression<f64> = Expression::parse_ctx(format, ctx).unwrap();
    let res: f64 = expr.eval().unwrap().unwrap_single();
    res
}