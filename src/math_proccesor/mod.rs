pub mod parse_float;

trait MathExpression{
    type ReturnType;

    fn run(&self) -> Self::ReturnType;
}