extern crate pretty_env_logger;
#[macro_use] extern crate log;

mod service;
mod math_proccesor;

use actix_web::{web, App, HttpServer};

#[actix_web::main]
async fn main() -> std::io::Result<()> {
    pretty_env_logger::init();
    
    HttpServer::new(|| {
        App::new()
            .route("/float", web::post().to(service::parse_float::run))
    })
    .bind("0.0.0.0:8080")?
    .run()
    .await
}
