use actix_web::{web, Result};
use serde::Deserialize;
use crate::math_proccesor;

use std::collections::HashMap;

#[derive(Deserialize)]
pub struct FloatExpression{
    pub formula: String,
    pub variables: HashMap<String, f64>
}

pub async fn run(float_expression: web::Json<FloatExpression>) -> Result<String> {
    info!("Parsing Float");

    let result = math_proccesor::parse_float::run(&float_expression.formula, &float_expression.variables);
    Ok(format!("Result:{}", result))
}

