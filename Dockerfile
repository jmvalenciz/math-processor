FROM alpine:latest

WORKDIR /usr/bin
COPY ./target/release/math-processor .

RUN apk add --no-cache libgcc m4

EXPOSE 8080
CMD ["math-processor"]
